var https = require('https');
var fs = require('fs');
var request = require('request');
var he = require('he');
var stamp = new Date().toJSON().replace(new RegExp(':', 'g'),'.');
//var sleep = require('sleep')
var resp = ''
var xml = '<?xml version="1.0" encoding="UTF-8"?>'
 xml = xml + '<soapenv:Envelope xmlns:com="http://servicecenter.peregrine.com/PWS/Common" xmlns:pws="http://servicecenter.peregrine.com/PWS" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'
 xml = xml + '<soapenv:Header/>'
 xml = xml + '  <soapenv:Body>'
 xml = xml + '    <pws:UpdateIncRequest ignoreEmptyElements="true" updateconstraint="-1">'
 xml = xml + '      <pws:model>'
 xml = xml + '        <pws:keys>'
 xml = xml + '          <pws:Number type="String">IM56688618</pws:Number>'
 xml = xml + '        </pws:keys>'
 xml = xml + '        <pws:instance>'
 xml = xml + '          <pws:Number type="String">IM56688618</pws:Number>'
 xml = xml + '          <pws:ExtId type="String"> </pws:ExtId>'
 xml = xml + '        </pws:instance>'
 xml = xml + '        <pws:messages> <com:message type="String"/>'
 xml = xml + '        </pws:messages>'
 xml = xml + '      </pws:model>'
 xml = xml + '      <Password xmlns="http://servicecenter.peregrine.com/PWS">654321</Password>'
 xml = xml + '    </pws:UpdateIncRequest>'
 xml = xml + '  </soapenv:Body>'
 xml = xml + '</soapenv:Envelope>'

const options = {
  host: '194.54.14.110',
  port: 1775, 
  path: '/',
  method: 'POST',
  rejectUnauthorized: false,

  key:  fs.readFileSync('./fokhtberger.key', 'utf8'),
  cert: fs.readFileSync('./fokhtberger.pem', 'utf8'), 
  headers: {
    'Content-Type': 'text/xml',
    'Content-Length': Buffer.byteLength( xml, 'utf8' )    
  }
};

console.log(' - URL=[https://' + options.host + ':' + options.port + options.path + '], method=[' + options.method + '] '); 

var req = https.request( options, function( res) {
  console.log( ' - statusCode: ' + res.statusCode );
  var data = '';
  res.on('data', function (d) {
          data += d;
			
  });
  res.on('end', function() {
	resp = data;	
	fs.writeFile('response ' +stamp+'.txt',he.decode(resp), (err) => {
			if (err) throw err;
			console.log('The file has been saved!');
		}
	);
	
  });

}).on('error', function (e) {
  console.log( ' - Request Error: ' + e.message );
});

req.write( xml );
req.end();


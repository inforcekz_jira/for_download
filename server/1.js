var https = require('https');
var fs = require('fs');
var convert = require('xml-js');
var request = require('request');
var he = require('he');
var parser = require('fast-xml-parser');
var inspect = require('eyes').inspector({maxLength: false});
var stamp = new Date().toJSON().replace(new RegExp(':', 'g'),'.');
//var sleep = require('sleep')
var resp = ''
var xml =       '<?xml version="1.0" encoding="UTF-8"?>';
    xml = xml + '<soapenv:Envelope xmlns:pws="http://servicecenter.peregrine.com/PWS" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">';
    xml = xml +    '<soapenv:Body>';
    xml = xml +       '<pws:ListRequest>';
    xml = xml +          '<pws:model>';
    xml = xml +             '<pws:keys></pws:keys>';
    xml = xml +             '<pws:instance></pws:instance>';
    xml = xml +          '</pws:model>';
    xml = xml +          '<pws:Password>654321</pws:Password>';
    xml = xml +       '</pws:ListRequest>';
    xml = xml +    '</soapenv:Body>';
    xml = xml + '</soapenv:Envelope>';


const options = {
  host: '194.54.14.110',
  port: 1775, 
  path: '/',
  method: 'POST',
  rejectUnauthorized: false,

  key:  fs.readFileSync('fokhtberger.key', 'utf8'),
  cert: fs.readFileSync('fokhtberger.pem', 'utf8'), 
  headers: {
    'Content-Type': 'text/xml',
    'Content-Length': Buffer.byteLength( xml, 'utf8' )    
  }
};
var parserOpts = {
    attributeNamePrefix : "@_",
    attrNodeName: "attr", //default is 'false'
    textNodeName : "#text",
    ignoreAttributes : true,
    ignoreNameSpace : false,
    allowBooleanAttributes : false,
    parseNodeValue : true,
    parseAttributeValue : false,
    trimValues: true,
    cdataTagName: "__cdata", //default is 'false'
    cdataPositionChar: "\\c",
    localeRange: "", //To support non english character in tag/attribute values.
    parseTrueNumberOnly: false,
    arrayMode: false, //"strict"
    attrValueProcessor: (val, attrName) => he.decode(val, {isAttributeValue: true}),//default is a=>a
    tagValueProcessor : (val, tagName) => he.decode(val), //default is a=>a
    stopNodes: ["parse-me-as-string"]
};

console.log(' - URL=[https://' + options.host + ':' + options.port + options.path + '], method=[' + options.method + '] '); 

var req = https.request( options, function( res) {
  console.log( ' - statusCode: ' + res.statusCode );
  var data = '';
  res.on('data', function (d) {
          data += d;
			
  });
  res.on('end', function() {
	resp = data;
	var result = convert.xml2js(resp, {compact: true, spaces: 4});
	inspect(result)
	//inspect(JSON.parse(resp));
	fs.writeFile('response ' +stamp+'.txt',he.decode(resp), (err) => {
			if (err) throw err;
			console.log('The file has been saved!');
		}
	);
	if( parser.validate(resp) === true) { //optional (it'll return an object in case it's not valid)
		inspect(resp)
		var jsonObj = parser.parse(resp,parserOpts);
		//console.dir(JSON.stringify(jsonObj));
	}	
	
  });

}).on('error', function (e) {
  console.log( ' - Request Error: ' + e.message );
});

req.write( xml );
req.end();


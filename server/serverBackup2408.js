// INF 30.01.2020 Сервер интеграции ПАО

//inbound:
/*
import groovyx.net.http.HTTPBuilder;
import static groovyx.net.http.ContentType.*;
import groovyx.net.http.ContentType;
import static groovyx.net.http.Method.*;

def http = new HTTPBuilder('myhost.com')
	http.request( POST ) {
		uri.path = '/'
		requestContentType = ContentType.JSON
		body = [title: issue.get("summary"), desc: issue.get("description")]
		log.info(body.title)
		response.success = { resp ->
		log.info( "POST response status: "+resp.statusLine+"}")
	}
}
*/	//groovy code

/*			Список полей
// SM field : customfield id : customfield type

Number 		12300	string
Status 		12401	string
Information 	Description
Description 	Summary
Assignment 	12402	string
ExtID 		12403	string
Koruswork 	12404	string
OpenTime 		12411	datetime picker
Resolution 	12406	string
CloCode 		12407	string
ContactID 	12408	string
DevSourceID 	12409	string
LocationID 	12410	string
INF_PAO_Source		12412	string	// флаг "мастер инцидент"
INF_PAO_CurrentDirection			12413	string	// флаг "текущее направление"
*/

var iconv = require('iconv-lite');
const express = require('express');
const app = express();
var xml2js = require('xml2js');
var convert = require('xml-js');
var inspect = require('eyes').inspector({maxLength: false})
var https = require('https');
var fs = require('fs');
var request = require('request');
var he = require('he');
var stamp = new Date().toJSON().replace(new RegExp(':', 'g'),'.');
var resp = ''
var test = {0:'post'};
const SimpleNodeLogger = require('simple-node-logger'),
    opts = {
        logFilePath:'log',
        timestampFormat:'YYYY-MM-DD HH:mm:ss.SSS'
    },
log = SimpleNodeLogger.createSimpleLogger( opts );
log.setLevel('info');
app.use(express.json())

//	маппинг ПАО - Jira Insight
//	LocationID : Сервис
const services = {
	'АКТАУ' : [{"key":"SERVICE-6"}],
	'АКТОБЕ' : [{"key":"SERVICE-7"}],
	'АЛМАТЫ' : [{"key":"SERVICE-8"}],
	'АТЫРАУ' : [{"key":"SERVICE-9"}],
	'КАРАГАНДА' : [{"key":"SERVICE-10"}],
	'КОКШЕТАУ' : [{"key":"SERVICE-11"}],
	'КОСТАНАЙ' : [{"key":"SERVICE-12"}],
	'КЫЗЫЛОРДА' : [{"key":"SERVICE-13"}],
	'НУР-СУЛТАН' : [{"key":"SERVICE-14"}],
	'ПАВЛОДАР' : [{"key":"SERVICE-15"}],
	'ПЕТРОПАВЛОВСК' : [{"key":"SERVICE-16"}],
	'ТАЛДЫКОРГАН' : [{"key":"SERVICE-17"}],
	'ТАРАЗ' : [{"key":"SERVICE-18"}],
	'ТУРКЕСТАН' : [{"key":"SERVICE-19"}],
	'УРАЛЬСК' : [{"key":"SERVICE-20"}],
	'УСТЬ-КАМЕНОГОРСК' : [{"key":"SERVICE-21"}],
	'ШЫМКЕНТ' : [{"key":"SERVICE-22"}]
}

const closureCodes = {
	'Выполнено' : '1',
	'Согласовано' : '1',
	'Done' : '1',
	'Готово' : '1',
	'Протестировано на Dev' : '1',
	'Не воспроизводится' : '2',
	'Отклонён' : '3',
	'Не выполнено' : '4',
	'Не согласовано' : '4',
	'Похожая' : '4',
	'Отложено' : '4',
	'Не будет выполнено' : '4',
	'Дубликат' : '4',
	'Не получится' : '4'
}



function setPaoExtId(extId, number) {
	var result;
	var xml = '<?xml version="1.0" encoding="UTF-8"?>'
	xml = xml + '<soapenv:Envelope xmlns:com="http://servicecenter.peregrine.com/PWS/Common" xmlns:pws="http://servicecenter.peregrine.com/PWS" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'
	xml = xml + '<soapenv:Header/>'
	xml = xml + '  <soapenv:Body>'
	xml = xml + '    <pws:UpdateIncRequest ignoreEmptyElements="true" updateconstraint="-1">'
	xml = xml + '      <pws:model>'
	xml = xml + '        <pws:keys>'
	xml = xml + '          <pws:Number type="String">'
	xml = xml + number;
	xml = xml + '</pws:Number>'
	xml = xml + '        </pws:keys>'
	xml = xml + '        <pws:instance>'
	xml = xml + '          <pws:Number type="String">'
	xml = xml + number;
	xml = xml + '</pws:Number>'
	xml = xml + '          <pws:ExtId type="String">'
	xml = xml + extId;
	xml = xml + '</pws:ExtId>'
	xml = xml + '        </pws:instance>'
	xml = xml + '        <pws:messages> <com:message type="String"/>'
	xml = xml + '        </pws:messages>'
	xml = xml + '      </pws:model>'
	xml = xml + '      <Password xmlns="http://servicecenter.peregrine.com/PWS">654321</Password>'
	xml = xml + '    </pws:UpdateIncRequest>'
	xml = xml + '  </soapenv:Body>'
	xml = xml + '</soapenv:Envelope>'
	var options = {
		host: '194.54.14.110',
		port: 1775,
		path: '/',
		method: 'POST',
		rejectUnauthorized: false,

		key:  fs.readFileSync('./fokhtberger.key', 'utf8'),
		cert: fs.readFileSync('./fokhtberger.pem', 'utf8'),
		headers: {
			'Content-Type': 'text/xml',
			'Content-Length': Buffer.byteLength( xml, 'utf8' )
		}
	};

	//console.log(' - URL=[https://' + options.host + ':' + options.port + options.path + '], method=[' + options.method + '] ');

	var req = https.request( options, function( res) {
		result = res.statusCode;
		//console.log( ' - statusCode: ' + res.statusCode );
		log.info("Original issue updated "+res.statusCode)
		var data = '';
		res.on('data', function (d) {
		     data += d;

		});
		res.on('end', function() {
		resp = data;
		/*fs.writeFile('response ' +stamp+'.txt',he.decode(resp), (err) => {
				if (err) throw err;
				console.log('The file has been saved!');
			}
		);*/
			//return result
		});

	}).on('error', function (e) {
		log.error( ' - Request Error: ' + e.message );
	});

	req.write( xml );
	req.end();
	//return result
}

function checkJira(paoIssue){
	var createdJiraIssueKey;
	var paoId = paoIssue['Number']['_text']
	var url = 'http://10.0.0.221:8080/rest/api/2/search?jql=cf[12300]~' 	// customfield id
	url = url + paoId											// customfield value
	// если инцидент в ПАО не закрыт
	if(paoIssue['Status']['_text'] != "6 Закрыт"){
		if(!paoIssue['ExtId']){
			// сразу создать инцидент в Jira
			log.info('Jira issue not found. Creating...');
			request.post('http://10.0.0.221:8080/rest/api/2/issue/', {
				 'auth': {
					'user': 'jadmin',
					'pass': 'kMzEehZW4Bmo',
					'sendImmediately': true
				  },
				json: {
					fields: {
						project : {
							key : "SD"
						},
						summary: paoIssue['Description'] ? paoIssue['Description']['_text'] : "",
						description : paoIssue['Information'] ? paoIssue['Information']['_text'] : "",
						customfield_13005: paoIssue['Description'] ? paoIssue['Description']['_text'] : "",
						customfield_13020 : paoIssue['Information'] ? paoIssue['Information']['_text'] : "",
						customfield_13002 : paoIssue['Number']['_text'],		// Number
						customfield_10218 : [{"key":"SERVICE-1085"}],	// категория
						customfield_10225 : [{"key":"SERVICE-1084"}],	// услуга
						//customfield_10226 : paoIssue['LocationID'] ? services[paoIssue['LocationID']['_text']] : "",	// сервис
						customfield_10226 : [{"key":"SERVICE-1083"}],	// сервис
						customfield_13003 : paoIssue['Status'] ? paoIssue['Status']['_text'] : "",
						customfield_13006 : paoIssue['Assignment'] ? paoIssue['Assignment']['_text'] : "",
						customfield_13007 : paoIssue['ExtId'] ? paoIssue['ExtId']['_text'] : "",
						customfield_13008 : paoIssue['KorusWork'] ? paoIssue['KorusWork']['_text'] : "",
						customfield_13019 : paoIssue['OpenTime'] ? paoIssue['OpenTime']['_text'] : "",
						customfield_13010 : paoIssue['Resolution'] ? paoIssue['Resolution']['_text'] : "",
						customfield_13011 : paoIssue['CloCode'] ? paoIssue['CloCode']['_text'] : "",
						customfield_13012 : paoIssue['ContactID'] ? paoIssue['ContactID']['_text'] : "",
						customfield_13013 : paoIssue['DevSourceID'] ? paoIssue['DevSourceID']['_text'] : "",
						customfield_13014 : paoIssue['LocationID'] ? paoIssue['LocationID']['_text'] : "",
						customfield_13018 : paoIssue['KorusTariffFlm'] ? paoIssue['KorusTariffFlm']['_text'] : "",
						customfield_13015 : paoIssue['AffectedCI'] ? paoIssue['AffectedCI']['_text'] : "",
						customfield_13017 : "PAO",	// Мастер инцидент
						customfield_13016 : "PAO",	// Мастер инцидент
						issuetype : {
							name: "Инцидент"
						}
					}
				}
			}, (error, res, body) => {
					if (error) {
						console.error(error)
						return
					}
					log.info("Jira issue created "+res.statusCode)
					console.dir(body['key'])
					console.dir(body);
						if(res.statusCode == "200" || res.statusCode == "201" || res.statusCode == "204"){
							createdJiraIssueKey = body['key'];
							log.info("Updating original issue. Set ExtId = "+createdJiraIssueKey);
							setPaoExtId(createdJiraIssueKey, paoId);
						}
				})
		}
		else{
			console.dir(url);
			request(url, {
				 'auth': {
					'user': 'jadmin',
					'pass': 'kMzEehZW4Bmo',
					'sendImmediately': true
				  }
			}, function (error, res, body){
				//console.dir('total '+body);
				//console.dir('Waiting for response...');
				//console.dir(JSON.parse(body)['issues'][0]['key']);
				if (error) {
					console.error(error)
					return
				}
				if(JSON.parse(body)['issues'][0]){
					var jiraIssueId = JSON.parse(body)['issues'][0]['key']
				}
				if(jiraIssueId){
					// инцидент в Jira найден
							let currentDirection = JSON.parse(body)['issues'][0]['fields']['customfield_12413']
							let jiraStatus = JSON.parse(body)['issues'][0]['fields']['status']['name'];
							let smStatus = paoIssue['Status']['_text'];
							log.info(jiraIssueId+" Jira issue found! Current direction = "+currentDirection+" . Jira status = "+jiraStatus+" . SM status = "+smStatus+" .Checking...")
							if(currentDirection == "jira"){
								if(smStatus == "5 Выполнен" && jiraStatus == "Решается на L3"){
									// обновить поля в Jira
									// сменить статус на "Ожидает подтверждения координатором"
									// сменить текушее направление
									log.info(jiraIssueId +' OK. Execute transition 271...')
									let transitionUrl = 'http://10.0.0.221:8080/rest/api/2/issue/'
									transitionUrl = transitionUrl + jiraIssueId
									transitionUrl = transitionUrl + '/transitions'
									request.post(transitionUrl, {
										 'auth': {
											'user': 'jadmin',
											'pass': 'kMzEehZW4Bmo',
											'sendImmediately': true
										  },
										json: {
											customfield_12506: paoIssue['Description'] ? paoIssue['Description']['_text'] : "",
											customfield_12507 : paoIssue['Information'] ? paoIssue['Information']['_text'] : "",
											customfield_12401 : paoIssue['Status'] ? paoIssue['Status']['_text'] : "",
											customfield_12402 : paoIssue['Assignment'] ? paoIssue['Assignment']['_text'] : "",
											customfield_12403 : paoIssue['ExtId'] ? paoIssue['ExtId']['_text'] : "",
											customfield_12502 : paoIssue['KorusWork'] ? paoIssue['KorusWork']['_text'] : "",
											customfield_12411 : paoIssue['OpenTime'] ? paoIssue['OpenTime']['_text'] : "",
											customfield_12406 : paoIssue['Resolution'] ? paoIssue['Resolution']['_text'] : "",
											customfield_12407 : paoIssue['CloCode'] ? paoIssue['CloCode']['_text'] : "",
											customfield_12408 : paoIssue['ContactID'] ? paoIssue['ContactID']['_text'] : "",
											customfield_12409 : paoIssue['DevSourceID'] ? paoIssue['DevSourceID']['_text'] : "",
											customfield_12410 : paoIssue['LocationID'] ? paoIssue['LocationID']['_text'] : "",
											customfield_12600 : paoIssue['KorusTariffFlm'] ? paoIssue['KorusTariffFlm']['_text'] : "",
											customfield_12505 : paoIssue['AffectedCI'] ? paoIssue['AffectedCI']['_text'] : "",
											customfield_12413 : "PAO",	// текущее направление
											transition : {
												id: "271"	// "Решение L3"
											}
										}
									}, (error, res, body) => {
											if (error) {
												console.error(error)
												return
											}
											log.info('Transition 271 '+res.statusCode)
										});
								}
								else if((smStatus == "3 В работе" || smStatus == "2 Назначен") && jiraStatus == "Ожидает  приемки  инициатором"){
									// обновить поля в Jira
									// сменить статус на "Ожидает координатора"
									// сменить текушее направление
									log.info(jiraIssueId+' OK. Execute transition 511...')
									let transitionUrl = 'http://10.0.0.221:8080/rest/api/2/issue/'
									transitionUrl = transitionUrl + jiraIssueId
									transitionUrl = transitionUrl + '/transitions'
									request.post(transitionUrl, {
										 'auth': {
											'user': 'jadmin',
											'pass': 'kMzEehZW4Bmo',
											'sendImmediately': true
										  },
										json: {
											customfield_12506: paoIssue['Description'] ? paoIssue['Description']['_text'] : "",
											customfield_12507 : paoIssue['Information'] ? paoIssue['Information']['_text'] : "",
											customfield_12401 : paoIssue['Status'] ? paoIssue['Status']['_text'] : "",
											customfield_12402 : paoIssue['Assignment'] ? paoIssue['Assignment']['_text'] : "",
											customfield_12403 : paoIssue['ExtId'] ? paoIssue['ExtId']['_text'] : "",
											customfield_12502 : paoIssue['KorusWork'] ? paoIssue['KorusWork']['_text'] : "",
											customfield_12411 : paoIssue['OpenTime'] ? paoIssue['OpenTime']['_text'] : "",
											customfield_12406 : paoIssue['Resolution'] ? paoIssue['Resolution']['_text'] : "",
											customfield_12407 : paoIssue['CloCode'] ? paoIssue['CloCode']['_text'] : "",
											customfield_12408 : paoIssue['ContactID'] ? paoIssue['ContactID']['_text'] : "",
											customfield_12409 : paoIssue['DevSourceID'] ? paoIssue['DevSourceID']['_text'] : "",
											customfield_12410 : paoIssue['LocationID'] ? paoIssue['LocationID']['_text'] : "",
											customfield_12600 : paoIssue['KorusTariffFlm'] ? paoIssue['KorusTariffFlm']['_text'] : "",
											customfield_12505 : paoIssue['AffectedCI'] ? paoIssue['AffectedCI']['_text'] : "",
											customfield_12413 : "PAO",	// текущее направление
											transition : {
												id: "511"	// "Направить координатору"
											}
										}
									}, (error, res, body) => {
											if (error) {
												console.error(error)
												return
											}
											log.info('Transition 511 '+res.statusCode)
										});
								}
							}
							// update all fields
							log.info('Updating all fields for '+jiraIssueId+'...')
							let updateFieldsURL = 'http://10.0.0.221:8080/rest/api/2/issue/'
							updateFieldsURL = updateFieldsURL + jiraIssueId
							request.put(updateFieldsURL, {
								 'auth': {
									'user': 'jadmin',
									'pass': 'kMzEehZW4Bmo',
									'sendImmediately': true
								  },
								json: {
									fields: {
										customfield_12506: paoIssue['Description'] ? paoIssue['Description']['_text'] : "",
										customfield_12507 : paoIssue['Information'] ? paoIssue['Information']['_text'] : "",
										customfield_10226 : paoIssue['LocationID'] ? services[paoIssue['LocationID']['_text']] : "",	// сервис
										customfield_12401 : paoIssue['Status'] ? paoIssue['Status']['_text'] : "",
										customfield_12402 : paoIssue['Assignment'] ? paoIssue['Assignment']['_text'] : "",
										customfield_12403 : paoIssue['ExtId'] ? paoIssue['ExtId']['_text'] : "",
										customfield_12502 : paoIssue['KorusWork'] ? paoIssue['KorusWork']['_text'] : "",
										customfield_12411 : paoIssue['OpenTime'] ? paoIssue['OpenTime']['_text'] : "",
										customfield_12406 : paoIssue['Resolution'] ? paoIssue['Resolution']['_text'] : "",
										customfield_12407 : paoIssue['CloCode'] ? paoIssue['CloCode']['_text'] : "",
										customfield_12408 : paoIssue['ContactID'] ? paoIssue['ContactID']['_text'] : "",
										customfield_12409 : paoIssue['DevSourceID'] ? paoIssue['DevSourceID']['_text'] : "",
										customfield_12600 : paoIssue['KorusTariffFlm'] ? paoIssue['KorusTariffFlm']['_text'] : "",
										customfield_12410 : paoIssue['LocationID'] ? paoIssue['LocationID']['_text'] : "",
										customfield_12505 : paoIssue['AffectedCI'] ? paoIssue['AffectedCI']['_text'] : ""
									}
								}
							}, (error, res, body) => {
									if (error) {
										console.error(error)
										return
									}
									console.dir(body)
									log.info(jiraIssueId +' Jira issue all fields updated '+res.statusCode)
								});
				}
				else {
					log.info('Jira issue not found for '+paoId);
				}
			});
		}
	}
	else{		// обработать закрытый инцидент
		if(paoIssue['ExtId']){		// обновить поля в существующем инциденте Jira
			let jiraIssueId = paoIssue['ExtId']['_text']
			log.info('PAO closed. Updating all fields for '+jiraIssueId+'...')
			let updateFieldsURL = 'http://10.0.0.221:8080/rest/api/2/issue/'
			updateFieldsURL = updateFieldsURL + jiraIssueId
			request.put(updateFieldsURL, {
				 'auth': {
					'user': 'jadmin',
					'pass': 'kMzEehZW4Bmo',
					'sendImmediately': true
				  },
				json: {
					fields: {
						customfield_12506: paoIssue['Description'] ? paoIssue['Description']['_text'] : "",
						customfield_12507 : paoIssue['Information'] ? paoIssue['Information']['_text'] : "",
						customfield_10226 : paoIssue['LocationID'] ? services[paoIssue['LocationID']['_text']] : "",	// сервис
						customfield_12401 : paoIssue['Status'] ? paoIssue['Status']['_text'] : "",
						customfield_12402 : paoIssue['Assignment'] ? paoIssue['Assignment']['_text'] : "",
						customfield_12403 : paoIssue['ExtId'] ? paoIssue['ExtId']['_text'] : "",
						customfield_12502 : paoIssue['KorusWork'] ? paoIssue['KorusWork']['_text'] : "",
						customfield_12411 : paoIssue['OpenTime'] ? paoIssue['OpenTime']['_text'] : "",
						customfield_12406 : paoIssue['Resolution'] ? paoIssue['Resolution']['_text'] : "",
						customfield_12407 : paoIssue['CloCode'] ? paoIssue['CloCode']['_text'] : "",
						customfield_12408 : paoIssue['ContactID'] ? paoIssue['ContactID']['_text'] : "",
						customfield_12409 : paoIssue['DevSourceID'] ? paoIssue['DevSourceID']['_text'] : "",
						customfield_12600 : paoIssue['KorusTariffFlm'] ? paoIssue['KorusTariffFlm']['_text'] : "",
						customfield_12410 : paoIssue['LocationID'] ? paoIssue['LocationID']['_text'] : "",
						customfield_12505 : paoIssue['AffectedCI'] ? paoIssue['AffectedCI']['_text'] : ""
					}
				}
			}, (error, res, body) => {
					if (error) {
						console.error(error)
						return
					}
					console.dir(body)
					log.info(jiraIssueId +' Jira issue all fields updated '+res.statusCode)
			});
		}
		// обновить инцидент в ПАО
		// после этого он никгода не вернется в list request
		let closeText =  paoIssue['Resolution']['_text'] + ' '
		var xml = '<?xml version="1.0" encoding="UTF-8"?>'
		xml = xml + '<soapenv:Envelope xmlns:com="http://servicecenter.peregrine.com/PWS/Common" xmlns:pws="http://servicecenter.peregrine.com/PWS" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'
		xml = xml + '<soapenv:Header/>'
		xml = xml + '  <soapenv:Body>'
		xml = xml + '    <pws:UpdateIncRequest ignoreEmptyElements="true" updateconstraint="-1">'
		xml = xml + '      <pws:model>'
		xml = xml + '        <pws:keys>'
		xml = xml + '          <pws:Number type="String">'
		xml = xml + paoIssue['Number']['_text'];
		xml = xml + '</pws:Number>'
		xml = xml + '        </pws:keys>'
		xml = xml + '        <pws:instance>'
		xml = xml + '          <pws:Number type="String">'
		xml = xml + paoIssue['Number']['_text'];
		xml = xml + '</pws:Number>'
		xml = xml + '          <pws:Resolution type="Array"> <pws:Resolution type="String">'
		xml = xml + closeText;
		xml = xml+'</pws:Resolution></pws:Resolution>'
		xml = xml + '        </pws:instance>'
		xml = xml + '        <pws:messages> <com:message type="String"/>'
		xml = xml + '        </pws:messages>'
		xml = xml + '      </pws:model>'
		xml = xml + '      <Password xmlns="http://servicecenter.peregrine.com/PWS">654321</Password>'
		xml = xml + '    </pws:UpdateIncRequest>'
		xml = xml + '  </soapenv:Body>'
		xml = xml + '</soapenv:Envelope>'


		const options = {
		  host: '194.54.14.110',
		  port: 1775,
		  path: '/',
		  method: 'POST',
		  rejectUnauthorized: false,

		  key:  fs.readFileSync('./fokhtberger.key', 'utf8'),
		  cert: fs.readFileSync('./fokhtberger.pem', 'utf8'),
		  headers: {
			'Content-Type': 'text/xml',
			'Content-Length': Buffer.byteLength( xml, 'utf8' )
		  }
		};


  		var req = https.request( options, function( res) {														// запрос в hpsm
		  log.info( 'PAO statusCode: ' + res.statusCode );
		  log.info(paoIssue['Number']['_text']+" "+res.headers['rq_uid'])
		  console.log(' - headers: '+res.headers)
		  var data = '';
		  res.on('data', function (d) {
				  data += d;

		  });
		  res.on('end', function() {
			resp = data;
			var result = convert.xml2js(resp, {compact: true, spaces: 4});
			inspect(result)
			//console.dir(result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['CreateIncResponse']['model']['instance']['Number']['_text']);
			fs.writeFile('update closed issue ' +stamp+'.txt',he.decode(resp), (err) => {
					if (err) throw err;
					console.log('The file has been saved!');
				}
			);
		  });

		}).on('error', function (e) {
		  log.error( 'PAO Request Error: ' + e.message );
		});
		req.write( xml );
		req.end();
	}
}

app.get('/', (req, res) => {
  res.send('a');
  console.log('request inbound');
});

app.get('/listRequest', (req, res) => {
	res.type('json')
	res.send('ok');
	log.info('List request inbound');
	// request opts
	let xml = '<?xml version="1.0" encoding="UTF-8"?>';
	xml = xml + '<soapenv:Envelope xmlns:pws="http://servicecenter.peregrine.com/PWS" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">';
	xml = xml +    '<soapenv:Body>';
	xml = xml +       '<pws:ListRequest>';
	xml = xml +          '<pws:model>';
	xml = xml +             '<pws:keys></pws:keys>';
	xml = xml +             '<pws:instance></pws:instance>';
	xml = xml +          '</pws:model>';
	xml = xml +          '<pws:Password>654321</pws:Password>';
	xml = xml +       '</pws:ListRequest>';
	xml = xml +    '</soapenv:Body>';
	xml = xml + '</soapenv:Envelope>';
	//console.dir(xml);
	let options = {
		host: '194.54.14.110',
		port: 1775,
		path: '/',
		method: 'POST',
		rejectUnauthorized: false,

		key:  fs.readFileSync('./fokhtberger.key', 'utf8'),
		cert: fs.readFileSync('./fokhtberger.pem', 'utf8'),
		headers: {
			'Content-Type': 'text/xml',
			'Content-Length': Buffer.byteLength( xml, 'utf8' )
		}
	};
	//console.dir(options);
	// request body

	//console.log(' - URL=[https://' + options.host + ':' + options.port + options.path + '], method=[' + options.method + '] ');
	//console.log('execute listRequest');
	var req = https.request( options, function( res) {
	  log.info( 'PAO ListRequest: ' + res.statusCode );
	  var data = '';
	  res.on('data', function (d) {
			  data += d;

	  });
	  res.on('end', function() {
		resp = data;
		//inspect(JSON.parse(resp));
		fs.writeFile('list ' +stamp+'.txt',he.decode(resp), (err) => {
				if (err) throw err;
				//console.log('The file has been saved!');
			}
		);
		fs.writeFile('list_headers ' +stamp+'.txt',JSON.stringify(res.headers), (err) => {
			if (err) throw err;
				console.log('headersThe file has been saved!');
			}
		);
		var result = convert.xml2js(resp, {compact: true, spaces: 4});
		
		if(result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['_attributes']['message'] != "(Больше) записей не найдено"){
			if(result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance'].length){
				log.info(result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance'].length + ' issues found. Starting...')
				for(let i = 0; i < result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance'].length; i++){
					log.info('Send '+ result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance'][i]['Number']['_text'] +' for check...')
						checkJira(result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance'][i])
				}
			}
			else{
				log.info('1 issue found. Starting...');
				log.info('Send '+ result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance']['Number']['_text'] +' for check...')
				checkJira(result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance']);
			}
		}
		else{
			log.info('(Больше) записей не найдено');
		}
		log.info("list request " +res.headers['rq_uid'])


	  });

	}).on('error', function (e) {
		log.error('Request Error: ' + e.message)
	  console.log( ' - Request Error: ' + e.message );
	});
	req.write( xml );
	req.end();
});

app.post('/',(req,res) => {
	res.send('test');
	console.log('post in');
	//console.dir(req.charset);
	console.dir(req.body);
	var xml =   '<soapenv:Envelope xmlns:com="http://servicecenter.peregrine.com/PWS/Common" xmlns:pws="http://servicecenter.peregrine.com/PWS" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'
	xml = xml + '   <soapenv:Header/>'
	xml = xml + '   <soapenv:Body>'
	xml = xml + '      <pws:CreateIncRequest ignoreEmptyElements="true" updateconstraint="-1">'
	xml = xml + '         <pws:model>'
	xml = xml + '            <pws:keys>'
	xml = xml + '               <pws:Number type="String"></pws:Number>'
	xml = xml + '            </pws:keys>'
	xml = xml + '            <pws:instance>'
	xml = xml + '               <pws:Number type="String"></pws:Number>'
	xml = xml + '               <pws:AffectedCI type="String">CI00865329</pws:AffectedCI>'					// хз че тут
	xml = xml + '               <pws:Information type="Array">'												// инфо
	xml = xml + '                  <pws:Information type="String">'
	xml = xml + 					req.body.title;
	xml = xml + ' '
	xml = xml + 					req.body.desc;
	xml = xml + '</pws:Information>'
	xml = xml + '               </pws:Information>'															// инфо
	xml = xml + '               <pws:ServiceIT type="String">CI00306611</pws:ServiceIT>'					// хз че тут
	xml = xml + '               <pws:Assignment type="String">Jira Казахстан (00016581)</pws:Assignment>'	// Jira Казахстан (00016581)
	xml = xml + '               <pws:IsUrgent type="Boolean">false</pws:IsUrgent>'
	xml = xml + '               <pws:ExtId type="String">'
	xml = xml + 					req.body.extId;
	xml = xml + '</pws:ExtId>'
	xml = xml + '               <pws:AkWorktype type="String">1</pws:AkWorktype>'							// хз че тут
	xml = xml + '            </pws:instance>'
	xml = xml + '            <pws:messages>'
	xml = xml + '               <com:message type="String"/>'
	xml = xml + '            </pws:messages>'
	xml = xml + '         </pws:model>'
	xml = xml + '         <pws:Password>654321</pws:Password>'												// 654321
	xml = xml + '      </pws:CreateIncRequest>'
	xml = xml + '   </soapenv:Body>'
	xml = xml + '</soapenv:Envelope>'



	const options = {
	  host: '194.54.14.110',
	  port: 1775,
	  path: '/',
	  method: 'POST',
	  rejectUnauthorized: false,

	  key:  fs.readFileSync('./fokhtberger.key', 'utf8'),
	  cert: fs.readFileSync('./fokhtberger.pem', 'utf8'),
	  headers: {
		'Content-Type': 'text/xml',
		'Content-Length': Buffer.byteLength( xml, 'utf8' )
	  }
	};


	var req = https.request( options, function( res) {														// запрос в hpsm
	  console.log( ' - statusCode: ' + res.statusCode );
	  var data = '';
	  res.on('data', function (d) {
			  data += d;

	  });
	  res.on('end', function() {
		resp = data;
		var result = convert.xml2js(resp, {compact: true, spaces: 4});
		inspect(result)
		//console.dir(result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['CreateIncResponse']['model']['instance']['Number']['_text']);
		fs.writeFile('response ' +stamp+'.txt',he.decode(resp), (err) => {
				if (err) throw err;
				console.log('The file has been saved!');
			}
		);

	  });

	}).on('error', function (e) {
	  console.log( ' - Request Error: ' + e.message );
	});

	req.write( xml );
	req.end();
});

app.post('/takeToWork',(req,response) => {
	log.info('Take to work inbound');
	//console.dir(req.charset);
	console.dir(req.body);
	var number = req.body.paoNumber;
	var jiraComment = ' ';
	if(req.body.jiraComment){
		jiraComment = req.body.jiraComment
	}
	var xml = '<?xml version="1.0" encoding="UTF-8"?>'
	xml = xml + '<soapenv:Envelope xmlns:com="http://servicecenter.peregrine.com/PWS/Common" xmlns:pws="http://servicecenter.peregrine.com/PWS" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'
	xml = xml + '<soapenv:Header/>'
	xml = xml + '  <soapenv:Body>'
	xml = xml + '    <pws:UpdateIncRequest ignoreEmptyElements="true" updateconstraint="-1">'
	xml = xml + '      <pws:model>'
	xml = xml + '        <pws:keys>'
	xml = xml + '          <pws:Number type="String">'
	xml = xml + number;
	xml = xml + '</pws:Number>'
	xml = xml + '        </pws:keys>'
	xml = xml + '        <pws:instance>'
	xml = xml + '          <pws:Number type="String">'
	xml = xml + number;
	xml = xml + '</pws:Number>'
	xml = xml + '          <pws:Resolution type="Array"> <pws:Resolution type="String">'
	xml = xml + req.body.jiraComment;
	xml = xml+'</pws:Resolution></pws:Resolution>'
	xml = xml + '          <pws:Status type="String">'
	xml = xml + '2';
	xml = xml + '</pws:Status>'
	xml = xml + '        </pws:instance>'
	xml = xml + '        <pws:messages> <com:message type="String"/>'
	xml = xml + '        </pws:messages>'
	xml = xml + '      </pws:model>'
	xml = xml + '      <Password xmlns="http://servicecenter.peregrine.com/PWS">654321</Password>'
	xml = xml + '    </pws:UpdateIncRequest>'
	xml = xml + '  </soapenv:Body>'
	xml = xml + '</soapenv:Envelope>'


	const options = {
	  host: '194.54.14.110',
	  port: 1775,
	  path: '/',
	  method: 'POST',
	  rejectUnauthorized: false,

	  key:  fs.readFileSync('./fokhtberger.key', 'utf8'),
	  cert: fs.readFileSync('./fokhtberger.pem', 'utf8'),
	  headers: {
		'Content-Type': 'text/xml',
		'Content-Length': Buffer.byteLength( xml, 'utf8' )
	  }
	};


	var req = https.request( options, function( res) {														// запрос в hpsm
	  log.info( 'PAO statusCode: ' + res.statusCode );
	  console.log(' - headers: '+res.headers)
	  var data = '';
	  res.on('data', function (d) {
			  data += d;

	  });
	  res.on('end', function() {
		resp = data;
		var result = convert.xml2js(resp, {compact: true, spaces: 4});
		inspect(result)
		//console.dir(result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['CreateIncResponse']['model']['instance']['Number']['_text']);
		fs.writeFile('take to work ' +stamp+'.txt',he.decode(resp), (err) => {
				if (err) throw err;
				console.log('The file has been saved!');
			}
		);
		fs.writeFile('take to work_headers ' +stamp+'.txt',JSON.stringify(res.headers), (err) => {
			if (err) throw err;
				console.log('headersThe file has been saved!');
			}
		);
			if(res.statusCode == 200 && (result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['UpdateIncResponse']['_attributes']['message'] == "Успешно")){
				response.send(res.statusCode)
				log.info("take to work "+number+" "+res.headers['rq_uid'])
			}
			else{
				response.send('bad request')
			}

	  });

	}).on('error', function (e) {
	  log.error( 'PAO Request Error: ' + e.message );
	  response.send('bad request')
	});

	req.write( xml );
	req.end();
});

app.post('/completeTask',(req,response) => {
	// коментарий закрытия : log.error (transientVars.get("comment"))
	log.info('Complete Task inbound');
	//console.dir(req.charset);
	console.dir(req.body);
	var number = req.body.paoNumber;
	var xml = '<?xml version="1.0" encoding="UTF-8"?>'
	xml = xml + '<soapenv:Envelope xmlns:com="http://servicecenter.peregrine.com/PWS/Common" xmlns:pws="http://servicecenter.peregrine.com/PWS" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'
	xml = xml + '<soapenv:Header/>'
	xml = xml + '  <soapenv:Body>'
	xml = xml + '    <pws:UpdateIncRequest ignoreEmptyElements="true" updateconstraint="-1">'
	xml = xml + '      <pws:model>'
	xml = xml + '        <pws:keys>'
	xml = xml + '          <pws:Number type="String">'
	xml = xml + number;
	xml = xml + '</pws:Number>'
	xml = xml + '        </pws:keys>'
	xml = xml + '        <pws:instance>'
	xml = xml + '          <pws:Number type="String">'
	xml = xml + number;
	xml = xml + '</pws:Number>'
	xml = xml + '<pws:CloCode type="String">'
	xml = xml + closureCodes[req.body.jiraResolution]
	xml = xml + '</pws:CloCode> '
	xml = xml + '          <pws:Resolution type="Array"> <pws:Resolution type="String">'
	xml = xml + req.body.jiraResolutionComment;
	xml = xml+'</pws:Resolution></pws:Resolution>'
	xml = xml + '          <pws:Status type="String">4</pws:Status>'
	xml = xml + '          <pws:ActualStart type="DateTime">'
	xml = xml + req.body.jiraStartTime;
	xml = xml + '</pws:ActualStart>'
    xml = xml + '<pws:ActualFinish type="DateTime">'
	xml = xml + req.body.jiraCloseTime;
	xml = xml + '</pws:ActualFinish>'
	xml = xml + ' <pws:ExtId type="String">'
	xml = xml + req.body.jiraKey;
	xml = xml + '</pws:ExtId>'
	xml = xml + '        </pws:instance>'
	xml = xml + '        <pws:messages> <com:message type="String"/>'
	xml = xml + '        </pws:messages>'
	xml = xml + '      </pws:model>'
	xml = xml + '      <Password xmlns="http://servicecenter.peregrine.com/PWS">654321</Password>'
	xml = xml + '    </pws:UpdateIncRequest>'
	xml = xml + '  </soapenv:Body>'
	xml = xml + '</soapenv:Envelope>'

	//console.dir('SEND TO PAO' +xml);
		fs.writeFile('send to pao complete task ' +stamp+'.xml',xml, (err) => {
				if (err) throw err;
				console.log('xmlThe file has been saved!');
			}
		);

	const options = {
	  host: '194.54.14.110',
	  port: 1775,
	  path: '/',
	  method: 'POST',
	  rejectUnauthorized: false,

	  key:  fs.readFileSync('./fokhtberger.key', 'utf8'),
	  cert: fs.readFileSync('./fokhtberger.pem', 'utf8'),
	  headers: {
		'Content-Type': 'text/xml',
		'Content-Length': Buffer.byteLength( xml, 'utf8' )
	  }
	};


	var req = https.request( options, function( res) {														// запрос в hpsm
	  log.info( 'PAO complete task: ' + res.statusCode );
	  	  inspect(res.headers);
		fs.writeFile('headers_complete ' +stamp+'.txt',JSON.stringify(res.headers), (err) => {
			if (err) throw err;
				console.log('headersThe file has been saved!');
			}
		);
	  var data = '';
	  res.on('data', function (d) {
			  data += d;

	  });
	  res.on('end', function() {
		resp = data;
		var result = convert.xml2js(resp, {compact: true, spaces: 4});
		//inspect(result)
		//console.dir(result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['CreateIncResponse']['model']['instance']['Number']['_text']);
		fs.writeFile('complete task response ' +stamp+'.txt',he.decode(resp), (err) => {
				if (err) throw err;
				console.log('The file has been saved!');
			}
		);
			if(res.statusCode == 200  && (result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['UpdateIncResponse']['_attributes']['message'] == "Успешно")){
				response.send(res.statusCode)
				log.info("complete task "+number+" "+res.headers['rq_uid'])
			}
			else{
				response.send('bad request')
			}
	  });

	}).on('error', function (e) {
	  log.error( 'PAO Request Error: ' + e.message );
	  response.send('bad request')
	});

	req.write( xml );
	req.end();
});

app.post('/createFromJira',(req,response) => {
	log.info('create From Jira');
	//console.dir(req.charset);
	console.dir(req.body);
	var xml =   '<soapenv:Envelope xmlns:com="http://servicecenter.peregrine.com/PWS/Common" xmlns:pws="http://servicecenter.peregrine.com/PWS" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'
	xml = xml + '   <soapenv:Header/>'
	xml = xml + '   <soapenv:Body>'
	xml = xml + '      <pws:CreateIncRequest ignoreEmptyElements="true" updateconstraint="-1">'
	xml = xml + '         <pws:model>'
	xml = xml + '            <pws:keys>'
	xml = xml + '               <pws:Number type="String"></pws:Number>'
	xml = xml + '            </pws:keys>'
	xml = xml + '            <pws:instance>'
	xml = xml + '               <pws:Number type="String"></pws:Number>'
	xml = xml + '               <pws:AffectedCI type="String">'
	xml = xml + req.body.jiraCI
	xml = xml + '</pws:AffectedCI>'
	xml = xml + '               <pws:Information type="Array">'							// description
	xml = xml + '                  <pws:Information type="String">'
	xml = xml + req.body.title
	xml = xml + '\n'
	xml = xml + req.body.desc
	xml = xml + '</pws:Information>'
	xml = xml + '               </pws:Information>'
	xml = xml + '               <pws:ServiceIT type="String">CI00306611</pws:ServiceIT>'		// всегда один сервис
	xml = xml + '               <pws:Assignment type="String">ЦУРУС 1-я линия КЗ (00016204)</pws:Assignment>'	// одна хуйня тут
	xml = xml + '               <pws:IsUrgent type="Boolean">false</pws:IsUrgent>'
	xml = xml + '               <pws:ExtId type="String">'
	xml = xml + req.body.jiraKey
	xml = xml + '</pws:ExtId>'			// jira id
	xml = xml + '               <pws:AkWorktype type="String">1</pws:AkWorktype>'
	xml = xml + '            </pws:instance>'
	xml = xml + '            <pws:messages>'
	xml = xml + '               <com:message type="String"/>'
	xml = xml + '            </pws:messages>'
	xml = xml + '         </pws:model>'
	xml = xml + '         <pws:Password>654321</pws:Password>'
	xml = xml + '      </pws:CreateIncRequest>'
	xml = xml + '   </soapenv:Body>'
	xml = xml + '</soapenv:Envelope>'

		fs.writeFile('createFromJira' +stamp+'.xml',xml, (err) => {
				if (err) throw err;
				console.log('xmlThe file has been saved!');
			}
		);
	const options = {
	  host: '194.54.14.110',
	  port: 1775,
	  path: '/',
	  method: 'POST',
	  rejectUnauthorized: false,

	  key:  fs.readFileSync('./fokhtberger.key', 'utf8'),
	  cert: fs.readFileSync('./fokhtberger.pem', 'utf8'),
	  headers: {
		'Content-Type': 'text/xml',
		'Content-Length': Buffer.byteLength( xml, 'utf8' )
	  }
	};


	var req = https.request( options, function( res) {														// запрос в hpsm
	  log.info( 'create From Jira: ' + res.statusCode );
	  var data = '';
	  res.on('data', function (d) {
			  data += d;

	  });
	  res.on('end', function() {
		resp = data;
		var result = convert.xml2js(resp, {compact: true, spaces: 4});
		console.dir(res.statusCode);
		fs.writeFile('create from jira headers ' +stamp+'.txt',JSON.stringify(res.headers), (err) => {
				if (err) throw err;
				console.log('The file has been saved!');
			}
		);
		fs.writeFile('create from jira response ' +stamp+'.txt',he.decode(resp), (err) => {
				if (err) throw err;
				console.log('The file has been saved!');
			}
		);
		if(res.statusCode == 200 && (result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['CreateIncResponse']['_attributes']['message'] == "Успешно")){
			response.send(result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['CreateIncResponse']['model']['keys']['Number']['_text'])
			//console.dir(result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['CreateIncResponse']['model']['keys']['Number']['_text'])
			log.info("create from Jira "+result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['CreateIncResponse']['model']['keys']['Number']['_text']+" "+res.headers['rq_uid'])
		}
		else{
			response.send('bad request')
		}
	  });

	}).on('error', function (e) {
	  log.error( 'PAO Request Error: ' + e.message );
	});

	req.write( xml );
	req.end();
});

app.post('/suspendTask',(req,response) => {
	log.info('suspendTask inbound');
	//console.dir(req.charset);
	console.dir(req.body);
	var number = req.body.paoNumber;
	var xml = '<?xml version="1.0" encoding="UTF-8"?>'
	xml = xml + '<soapenv:Envelope xmlns:com="http://servicecenter.peregrine.com/PWS/Common" xmlns:pws="http://servicecenter.peregrine.com/PWS" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'
	xml = xml + '<soapenv:Header/>'
	xml = xml + '  <soapenv:Body>'
	xml = xml + '    <pws:UpdateIncRequest ignoreEmptyElements="true" updateconstraint="-1">'
	xml = xml + '      <pws:model>'
	xml = xml + '        <pws:keys>'
	xml = xml + '          <pws:Number type="String">'
	xml = xml + number;
	xml = xml + '</pws:Number>'
	xml = xml + '        </pws:keys>'
	xml = xml + '        <pws:instance>'
	xml = xml + '          <pws:Number type="String">'
	xml = xml + number;
	xml = xml + '</pws:Number>'
	xml = xml + '          <pws:Resolution type="Array"> <pws:Resolution type="String">'
	xml = xml + req.body.jiraResolution;
	xml = xml+'</pws:Resolution></pws:Resolution>'
	xml = xml + '          <pws:Status type="String">3</pws:Status>'
	xml = xml + '        </pws:instance>'
	xml = xml + '        <pws:messages> <com:message type="String"/>'
	xml = xml + '        </pws:messages>'
	xml = xml + '      </pws:model>'
	xml = xml + '      <Password xmlns="http://servicecenter.peregrine.com/PWS">654321</Password>'
	xml = xml + '    </pws:UpdateIncRequest>'
	xml = xml + '  </soapenv:Body>'
	xml = xml + '</soapenv:Envelope>'


	const options = {
	  host: '194.54.14.110',
	  port: 1775,
	  path: '/',
	  method: 'POST',
	  rejectUnauthorized: false,

	  key:  fs.readFileSync('./fokhtberger.key', 'utf8'),
	  cert: fs.readFileSync('./fokhtberger.pem', 'utf8'),
	  headers: {
		'Content-Type': 'text/xml',
		'Content-Length': Buffer.byteLength( xml, 'utf8' )
	  }
	};


	var req = https.request( options, function( res) {														// запрос в hpsm
	  log.info( 'PAO statusCode: ' + res.statusCode );
	  inspect(res.headers);
		fs.writeFile('headers_suspend ' +stamp+'.txt',JSON.stringify(res.headers), (err) => {
			if (err) throw err;
				console.log('headersThe file has been saved!');
			}
		);
	  var data = '';
	  res.on('data', function (d) {
			  data += d;

	  });
	  res.on('end', function() {
		resp = data;
		var result = convert.xml2js(resp, {compact: true, spaces: 4});
		inspect(result)
		//console.dir(result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['CreateIncResponse']['model']['instance']['Number']['_text']);
		fs.writeFile('suspend ' +stamp+'.txt',he.decode(resp), (err) => {
				if (err) throw err;
				console.log('The file has been saved!');
			}
		);
			if(res.statusCode == 200  && (result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['UpdateIncResponse']['_attributes']['message'] == "Успешно")){
				response.send(res.statusCode)
				log.info("suspend task "+number+" "+res.headers['rq_uid'])
			}
			else{
				response.send('bad request')
			}
	  });

	}).on('error', function (e) {
	  log.error( 'PAO Request Error: ' + e.message );
	  response.send('bad request')
	});

	req.write( xml );
	req.end();
});

app.post('/unsuspendTask',(req,response) => {
	log.info('unsuspendTask inbound');
	//console.dir(req.charset);
	console.dir(req.body);
	var number = req.body.paoNumber;
	var xml = '<?xml version="1.0" encoding="UTF-8"?>'
	xml = xml + '<soapenv:Envelope xmlns:com="http://servicecenter.peregrine.com/PWS/Common" xmlns:pws="http://servicecenter.peregrine.com/PWS" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'
	xml = xml + '<soapenv:Header/>'
	xml = xml + '  <soapenv:Body>'
	xml = xml + '    <pws:UpdateIncRequest ignoreEmptyElements="true" updateconstraint="-1">'
	xml = xml + '      <pws:model>'
	xml = xml + '        <pws:keys>'
	xml = xml + '          <pws:Number type="String">'
	xml = xml + number;
	xml = xml + '</pws:Number>'
	xml = xml + '        </pws:keys>'
	xml = xml + '        <pws:instance>'
	xml = xml + '          <pws:Number type="String">'
	xml = xml + number;
	xml = xml + '</pws:Number>'
	xml = xml + '          <pws:Status type="String">2</pws:Status>'
	xml = xml + '        </pws:instance>'
	xml = xml + '        <pws:messages> <com:message type="String"/>'
	xml = xml + '        </pws:messages>'
	xml = xml + '      </pws:model>'
	xml = xml + '      <Password xmlns="http://servicecenter.peregrine.com/PWS">654321</Password>'
	xml = xml + '    </pws:UpdateIncRequest>'
	xml = xml + '  </soapenv:Body>'
	xml = xml + '</soapenv:Envelope>'


	const options = {
	  host: '194.54.14.110',
	  port: 1775,
	  path: '/',
	  method: 'POST',
	  rejectUnauthorized: false,

	  key:  fs.readFileSync('./fokhtberger.key', 'utf8'),
	  cert: fs.readFileSync('./fokhtberger.pem', 'utf8'),
	  headers: {
		'Content-Type': 'text/xml',
		'Content-Length': Buffer.byteLength( xml, 'utf8' )
	  }
	};


	var req = https.request( options, function( res) {														// запрос в hpsm
	  log.info( 'PAO statusCode: ' + res.statusCode );
	  inspect(res.headers);
		fs.writeFile('headers_unsuspend ' +stamp+'.txt',JSON.stringify(res.headers), (err) => {
			if (err) throw err;
				console.log('headersThe file has been saved!');
			}
		);
	  var data = '';
	  res.on('data', function (d) {
			  data += d;

	  });
	  res.on('end', function() {
		resp = data;
		var result = convert.xml2js(resp, {compact: true, spaces: 4});
		inspect(result)
		//console.dir(result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['CreateIncResponse']['model']['instance']['Number']['_text']);
		fs.writeFile('unsuspend ' +stamp+'.txt',he.decode(resp), (err) => {
				if (err) throw err;
				console.log('The file has been saved!');
			}
		);
			if(res.statusCode == 200  && (result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['UpdateIncResponse']['_attributes']['message'] == "Успешно")){
				response.send(res.statusCode)
				log.info("unsuspend "+number+" "+res.headers['rq_uid'])
			}
			else{
				response.send('bad request')
			}
	  });

	}).on('error', function (e) {
	 log.info( 'PAO Request Error: ' + e.message );
	 response.send('bad request')
	});

	req.write( xml );
	req.end();
});

app.post('/abortCreating',(req,response) => {
	log.info('abortCreating inbound');
	//console.dir(req.charset);
	console.dir(req.body);
	var number = req.body.paoNumber;
	var xml = '<?xml version="1.0" encoding="UTF-8"?>'
	xml = xml + '<soapenv:Envelope xmlns:com="http://servicecenter.peregrine.com/PWS/Common" xmlns:pws="http://servicecenter.peregrine.com/PWS" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'
	xml = xml + '<soapenv:Header/>'
	xml = xml + '  <soapenv:Body>'
	xml = xml + '    <pws:UpdateIncRequest ignoreEmptyElements="true" updateconstraint="-1">'
	xml = xml + '      <pws:model>'
	xml = xml + '        <pws:keys>'
	xml = xml + '          <pws:Number type="String">'
	xml = xml + number;
	xml = xml + '</pws:Number>'
	xml = xml + '        </pws:keys>'
	xml = xml + '        <pws:instance>'
	xml = xml + '          <pws:Number type="String">'
	xml = xml + number;
	xml = xml + '</pws:Number>'
	xml = xml + '<pws:CloCode type="String">3</pws:CloCode>'
	xml = xml + '          <pws:Resolution type="Array"> <pws:Resolution type="String">'
	xml = xml + req.body.jiraComment;
	xml = xml+'</pws:Resolution></pws:Resolution>'
	xml = xml + '          <pws:Status type="String">4</pws:Status>'
	xml = xml + '        </pws:instance>'
	xml = xml + '        <pws:messages> <com:message type="String"/>'
	xml = xml + '        </pws:messages>'
	xml = xml + '      </pws:model>'
	xml = xml + '      <Password xmlns="http://servicecenter.peregrine.com/PWS">654321</Password>'
	xml = xml + '    </pws:UpdateIncRequest>'
	xml = xml + '  </soapenv:Body>'
	xml = xml + '</soapenv:Envelope>'


	const options = {
	  host: '194.54.14.110',
	  port: 1775,
	  path: '/',
	  method: 'POST',
	  rejectUnauthorized: false,

	  key:  fs.readFileSync('./fokhtberger.key', 'utf8'),
	  cert: fs.readFileSync('./fokhtberger.pem', 'utf8'),
	  headers: {
		'Content-Type': 'text/xml',
		'Content-Length': Buffer.byteLength( xml, 'utf8' )
	  }
	};


	var req = https.request( options, function( res) {														// запрос в hpsm
	  log.info( 'PAO statusCode: ' + res.statusCode );
	  inspect(res.headers);
		fs.writeFile('headers_abortCreating ' +stamp+'.txt',JSON.stringify(res.headers), (err) => {
			if (err) throw err;
				console.log('headersThe file has been saved!');
			}
		);
	  var data = '';
	  res.on('data', function (d) {
			  data += d;

	  });
	  res.on('end', function() {
		resp = data;
		var result = convert.xml2js(resp, {compact: true, spaces: 4});
		inspect(result)
		//console.dir(result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['CreateIncResponse']['model']['instance']['Number']['_text']);
		fs.writeFile('abortCreating ' +stamp+'.txt',he.decode(resp), (err) => {
				if (err) throw err;
				console.log('The file has been saved!');
			}
		);
			if(res.statusCode == 200  && (result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['UpdateIncResponse']['_attributes']['message'] == "Успешно")){
				response.send(res.statusCode)
			}
			else{
				response.send('bad request')
			}
	  });

	}).on('error', function (e) {
	  log.error( 'PAO Request Error: ' + e.message );
	  response.send('bad request')
	});

	req.write( xml );
	req.end();
});

app.post('/closeTask',(req,response) => {
	// коментарий закрытия : log.error (transientVars.get("comment"))
	log.info('close Task inbound');
	//console.dir(req.charset);
	console.dir(req.body);
	var number = req.body.paoNumber;
	var xml = '<?xml version="1.0" encoding="UTF-8"?>'
	xml = xml + '<soapenv:Envelope xmlns:com="http://servicecenter.peregrine.com/PWS/Common" xmlns:pws="http://servicecenter.peregrine.com/PWS" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'
	xml = xml + '<soapenv:Header/>'
	xml = xml + '  <soapenv:Body>'
	xml = xml + '    <pws:UpdateIncRequest ignoreEmptyElements="true" updateconstraint="-1">'
	xml = xml + '      <pws:model>'
	xml = xml + '        <pws:keys>'
	xml = xml + '          <pws:Number type="String">'
	xml = xml + number;
	xml = xml + '</pws:Number>'
	xml = xml + '        </pws:keys>'
	xml = xml + '        <pws:instance>'
	xml = xml + '          <pws:Number type="String">'
	xml = xml + number;
	xml = xml + '</pws:Number>'
	xml = xml + '<pws:CloCode type="String">1</pws:CloCode>'
	xml = xml + '          <pws:Resolution type="Array"> <pws:Resolution type="String">'
	xml = xml + req.body.jiraCloseComment;
	xml = xml+'</pws:Resolution></pws:Resolution>'
	xml = xml + '          <pws:Status type="String">5</pws:Status>'
	xml = xml + '        </pws:instance>'
	xml = xml + '        <pws:messages> <com:message type="String"/>'
	xml = xml + '        </pws:messages>'
	xml = xml + '      </pws:model>'
	xml = xml + '      <Password xmlns="http://servicecenter.peregrine.com/PWS">654321</Password>'
	xml = xml + '    </pws:UpdateIncRequest>'
	xml = xml + '  </soapenv:Body>'
	xml = xml + '</soapenv:Envelope>'

	//console.dir('SEND TO PAO' +xml);
		fs.writeFile('send to pao close task ' +stamp+'.xml',xml, (err) => {
				if (err) throw err;
				console.log('xmlThe file has been saved!');
			}
		);

	const options = {
	  host: '194.54.14.110',
	  port: 1775,
	  path: '/',
	  method: 'POST',
	  rejectUnauthorized: false,

	  key:  fs.readFileSync('./fokhtberger.key', 'utf8'),
	  cert: fs.readFileSync('./fokhtberger.pem', 'utf8'),
	  headers: {
		'Content-Type': 'text/xml',
		'Content-Length': Buffer.byteLength( xml, 'utf8' )
	  }
	};


	var req = https.request( options, function( res) {														// запрос в hpsm
	  log.info( 'PAO close task: ' + res.statusCode );
	  	  inspect(res.headers);
		fs.writeFile('headers_close ' +stamp+'.txt',JSON.stringify(res.headers), (err) => {
			if (err) throw err;
				console.log('headersThe file has been saved!');
			}
		);
	  var data = '';
	  res.on('data', function (d) {
			  data += d;

	  });
	  res.on('end', function() {
		resp = data;
		var result = convert.xml2js(resp, {compact: true, spaces: 4});
		//inspect(result)
		//console.dir(result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['CreateIncResponse']['model']['instance']['Number']['_text']);
		fs.writeFile('close task response ' +stamp+'.txt',he.decode(resp), (err) => {
				if (err) throw err;
				console.log('The file has been saved!');
			}
		);
			if(res.statusCode == 200  && (result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['UpdateIncResponse']['_attributes']['message'] == "Успешно")){
				response.send(res.statusCode)
				//log.info("close task "+number+" "+res.headers['rq_uid'])
			}
			else{
				response.send('bad request')
			}
			log.info("close task "+number+" "+res.headers['rq_uid'])
	  });

	}).on('error', function (e) {
	  log.error( 'PAO Request Error: ' + e.message );
	  response.send('bad request')
	});

	req.write( xml );
	req.end();
});
// Listen to the App Engine-specified port, or 8080 otherwise
const PORT = process.env.PORT || 30000;
app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}...`);
});

var https = require('https');
var http = require('http');
var fs = require('fs');
var request = require('request');
var he = require('he');
var convert = require('xml-js');
var inspect = require('eyes').inspector({maxLength: false})
var stamp = new Date().toJSON().replace(new RegExp(':', 'g'),'.');
//var sleep = require('sleep')
var resp = ''
var xml =       '<?xml version="1.0" encoding="UTF-8"?>';
    xml = xml + '<soapenv:Envelope xmlns:pws="http://servicecenter.peregrine.com/PWS" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">';
    xml = xml +    '<soapenv:Body>';
    xml = xml +       '<pws:ListRequest>';
    xml = xml +          '<pws:model>';
    xml = xml +             '<pws:keys></pws:keys>';
    xml = xml +             '<pws:instance></pws:instance>';
    xml = xml +          '</pws:model>';
    xml = xml +          '<pws:Password>654321</pws:Password>';
    xml = xml +       '</pws:ListRequest>';
    xml = xml +    '</soapenv:Body>';
    xml = xml + '</soapenv:Envelope>';


const services = {
	'АКТАУ' : [{"key":"SERVICE-6"}],
	'АКТОБЕ' : [{"key":"SERVICE-7"}],
	'АЛМАТЫ' : [{"key":"SERVICE-8"}],
	'АТЫРАУ' : [{"key":"SERVICE-9"}],
	'КАРАГАНДА' : [{"key":"SERVICE-10"}],
	'КОКШЕТАУ' : [{"key":"SERVICE-11"}],
	'КОСТАНАЙ' : [{"key":"SERVICE-12"}],
	'КЫЗЫЛОРДА' : [{"key":"SERVICE-13"}],
	'НУР-СУЛТАН' : [{"key":"SERVICE-14"}],
	'ПАВЛОДАР' : [{"key":"SERVICE-15"}],
	'ПЕТРОПАВЛОВСК' : [{"key":"SERVICE-16"}],
	'ТАЛДЫКОРГАН' : [{"key":"SERVICE-17"}],
	'ТАРАЗ' : [{"key":"SERVICE-18"}],
	'ТУРКЕСТАН' : [{"key":"SERVICE-19"}],
	'УРАЛЬСК' : [{"key":"SERVICE-20"}],
	'УСТЬ-КАМЕНОГОРСК' : [{"key":"SERVICE-21"}],
	'ШЫМКЕНТ' : [{"key":"SERVICE-22"}]
}


const options = {
  host: '194.54.14.110',
  port: 1775,
  path: '/',
  method: 'POST',
  rejectUnauthorized: false,

  key:  fs.readFileSync('./fokhtberger.key', 'utf8'),
  cert: fs.readFileSync('./fokhtberger.pem', 'utf8'),
  headers: {
    'Content-Type': 'text/xml',
    'Content-Length': Buffer.byteLength( xml, 'utf8' )
  }
};

//console.log(' - URL=[https://' + options.host + ':' + options.port + options.path + '], method=[' + options.method + '] ');
var count = 0;
	count++;
	console.log('start '+count)
	var createNumber = [];
	var createDescription = [];
	var createInformation = [];
	var req = https.request( options, function( res) {
	  console.log( ' - statusCode: ' + res.statusCode );
	  var data = '';
	  res.on('data', function (d) {
			  data += d;

	  });
	  res.on('end', function() {
		resp = data;
		var result = convert.xml2js(resp, {compact: true, spaces: 4});
		//inspect(result);
		for(let i = 0; i < result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance'].length; i++){
			console.log(i);
			createNumber.push(result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance'][i]['Number']['_text']);	// Number
			createDescription.push(result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance'][i]['Description']['_text'])	// Description
			createInformation.push(result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance'][i]['Information']['_text'])	// Information
		}
		console.dir(createDescription);
		for (let i = 0; i < createNumber.length; i++){
				request.post('http://10.0.0.221:8080/rest/api/2/issue/', {
					 'auth': {
						'user': 'jadmin',
						'pass': 'kMzEehZW4Bmo',
						'sendImmediately': true
					  },
					json: {
						fields: {
							project : {
								key : "SD"
							},
							/*			Список полей

							Number type		12300	string
							Status type		12401	string
							Information type	Description
							Description type	Summary
							Assignment type	12402	string
							ExtID type		12403	string
							Koruswork type		12404	string
							OpenTime type		12405	datetime picker
							Resolution type	12406	string
							CloCode type		12407	string
							ContactID type		12408	string
							DevSourceID type	12409	string
							LocationID type	12410	string
							Source			12412	string
							*/
							summary: createDescription[i],
							description : createInformation[i],
							customfield_12300 : createNumber[i],		// Number
							customfield_10218 : [{"key":"SERVICE-4"}],	// категория
							customfield_10225 : [{"key":"SERVICE-5"}],	// услуга
							customfield_10226 : services[result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance'][i]['LocationID']['_text']],	// сервис
							customfield_12401 : result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance'][i]['Status'] ? result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance'][i]['Status']['_text'] : " ",
							customfield_12402 : result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance'][i]['Assignment'] ? result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance'][i]['Assignment']['_text'] : " ",
							customfield_12403 : result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance'][i]['ExtId'] ? result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance'][i]['ExtId']['_text'] : " ",
							customfield_12404 : result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance'][i]['KorusWork'] ? result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance'][i]['KorusWork']['_text'] : " ",
							customfield_12411 : result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance'][i]['OpenTime'] ? result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance'][i]['OpenTime']['_text'] : " ",
							customfield_12406 : result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance'][i]['Resolution'] ? result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance'][i]['Resolution']['_text'] : " ",
							customfield_12407 : result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance'][i]['CloCode'] ? result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance'][i]['CloCode']['_text'] : " ",
							customfield_12408 : result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance'][i]['ContactID'] ? result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance'][i]['ContactID']['_text'] : " ",
							customfield_12409 : result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance'][i]['DevSourceID'] ? result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance'][i]['DevSourceID']['_text'] : " ",
							customfield_12410 : result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance'][i]['LocationID'] ? result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ListResponse']['instance'][i]['LocationID']['_text'] : " ",
							customfield_12412 : "PAO",
							issuetype : {
								name: "Инцидент"
							}
						}
					}
				}, (error, res, body) => {
						console.dir('create status '+ res.statusCode);
						//console.dir('create response ');
						//inspect(body);
						if (error) {
							console.error(error)
							return
						}
					})
		}
	  });
	}).on('error', function (e) {
	  console.log( ' - Request Error: ' + e.message );
	});

	req.write( xml );
	req.end();
